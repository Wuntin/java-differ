/*
 * Copyright 2019 Magnus Hultin
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.hultin.differ;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UncheckedIOException;


public class SerializableSerializer<T extends Serializable> implements Serializer<T> {
    private static final ThreadLocal<StreamPair> streamPair =
        new ThreadLocal<StreamPair>() {
            @Override protected StreamPair initialValue() {
                return new StreamPair();
            }
        };

    public String serialize(T object) {
        ByteArrayOutputStream stringStream = streamPair.get().stringStream;
        ObjectOutputStream objectStream = streamPair.get().objectStream;

        try {
            stringStream.reset();
            objectStream.reset();
            objectStream.writeObject(object);

            return stringStream.toString();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}

class StreamPair {
    protected ByteArrayOutputStream stringStream;
    protected ObjectOutputStream objectStream;

    public StreamPair() {
        try {
            this.stringStream = new ByteArrayOutputStream();
            this.objectStream = new ObjectOutputStream(this.stringStream);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
