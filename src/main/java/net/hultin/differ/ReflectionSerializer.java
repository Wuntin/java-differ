/*
 * Copyright 2019 Magnus Hultin
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.hultin.differ;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


public class ReflectionSerializer<T> implements Serializer<T> {
    private static final String FIELD_DELIMITER = ":";
    private static final String EQUALS_DELIMITER = "=";
    private static final String NULL_VALUE = "NULL";
    private static final String VISITED_VALUE = "VISITED";

    private Collection<String> ignoreFields;

    public ReflectionSerializer() {
        this.ignoreFields = new HashSet<>();
    }

    public ReflectionSerializer(Collection<String> ignoreFields) {
        this.ignoreFields = ignoreFields;
    }

    public String serialize(T object) {
        return this.serializeObject(object, new HashSet<Object>());
    }

    public String serializeObject(Object object, Collection<Object> visitedObjects) {
        ClassDescription classDesc = ClassDescription.forClass(object.getClass());
        StringBuffer buf = new StringBuffer();

        if (visitedObjects.contains(object)) {
            return VISITED_VALUE;
        }

        visitedObjects.add(object);

        for (String fieldName : classDesc.fieldNames()) {
            if (this.ignoreFields.contains(fieldName)) {
                continue;
            }

            if (buf.length() > 0) {
                buf.append(FIELD_DELIMITER);
            }

            buf.append(this.serializeField(classDesc, object, fieldName, visitedObjects));
        }

        return buf.toString();
    }

    private String serializeField(ClassDescription classDesc, Object object, String fieldName, Collection<Object> visitedObjects) {
        StringBuffer buf = new StringBuffer();
        Object ob = classDesc.extractField(object, fieldName);

        if (fieldName.equals("this$0")) {
            // Ignore the containing class for inner classes. Is the name *always* this$0 ?
            return "";
        }

        buf.append(fieldName + EQUALS_DELIMITER);

        if (ob == null) {
            buf.append(NULL_VALUE);
        } else if (classDesc.isPrimitive(fieldName)) {
            buf.append(ob.toString());
        } else {
            buf.append(this.serializeObject(ob, visitedObjects));
        }

        return buf.toString();
    }
}
