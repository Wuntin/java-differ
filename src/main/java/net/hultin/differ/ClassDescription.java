/*
 * Copyright 2019 Magnus Hultin
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.hultin.differ;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Map;
import java.util.Set;


public class ClassDescription {
    private static final Map<Class,ClassDescription> classDescriptions = new HashMap<>();

    public static ClassDescription forClass(Class<?> clazz) {
        if (! classDescriptions.containsKey(clazz)) {
            classDescriptions.put(clazz, new ClassDescription(clazz));
        }

        return classDescriptions.get(clazz);
    }

    private Class clazz;
    private Map<String,Field> fields;

    private ClassDescription(Class<?> clazz) {
        this.clazz = clazz;
        this.fields = createFieldMap(clazz);
    }

    private Map<String,Field> createFieldMap(Class<?> clazz) {
        Map<String,Field> map;
        Class<?> superclass = clazz.getSuperclass();

        if (superclass != null) {
            ClassDescription classDescription = ClassDescription.forClass(superclass);

            map = new TreeMap<String,Field>(classDescription.fields);
        } else {
            map = new TreeMap<String,Field>();
        }

        for (Field field : clazz.getDeclaredFields()) {
            map.put(field.getName(), field);
        }

        return map;
    }

    public Set<String> fieldNames() {
        return this.fields.keySet();
    }

    public boolean isPrimitive(String fieldName) {
        Field field = fields.get(fieldName);
        Class<?> clazz = field.getType();

        if (clazz.getName().equals("java.lang.String")) {
            return true;
        }
        
        return clazz.isPrimitive();
    }

    public Object extractField(Object object, String fieldName) {
        Field field = fields.get(fieldName);
        boolean accessibility = field.isAccessible();
        Object value = null;
        
        field.setAccessible(true);

        try {
            value = field.get(object);
        } catch (IllegalAccessException e) {
            // Should not happen?
        }

        field.setAccessible(accessibility);

        return value;
    }
}
