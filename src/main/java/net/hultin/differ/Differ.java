/*
 * Copyright 2019 Magnus Hultin
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.hultin.differ;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class Differ<T> {
    private Serializer<T> serializer = null;

    public Differ() {
        this.serializer = new ReflectionSerializer<T>();
    }

    public Differ(Serializer<T> serializer) {
        this.serializer = serializer;
    }

    public Differ(Collection<String> ignoreFields) {
        this.serializer = new ReflectionSerializer<T>(ignoreFields);
    }

    public DiffResult<T> diff(Collection<T> oldSet, Collection<T> newSet) {
        Map<String,Set<T>> oldMap = createMap(oldSet);
        Set<T> removedItems = new HashSet<>();
        Set<T> addedItems = new HashSet<>();

        for (T item : newSet) {
            if (item != null && !removeItemFromMap(item, oldMap)) {
                addedItems.add(item);
            }
        }

        for (Set<T> set : oldMap.values()) {
            removedItems.addAll(set);
        }

        return new DiffResult<T>(removedItems, addedItems);
    }

    private Map<String,Set<T>> createMap(Collection<T> set) {
        Map<String,Set<T>> map = new HashMap<>();

        for (T item : set) {
            if (item != null) {
                String key = serializer.serialize(item);

                if (!map.containsKey(key)) {
                    map.put(key, new HashSet<>());
                }

                map.get(key).add(item);
            }
        }

        return map;
    }

    private boolean removeItemFromMap(T item, Map<String,Set<T>> map) {
        String key = serializer.serialize(item);

        if (map.containsKey(key)) {
            Set<T> items = map.get(key);

            if (items.size() > 1) {
                // There's more than one item that serializes to the same key.
                // Should do something more intelligent.
                T otherItem = items.iterator().next();

                items.remove(otherItem);

                return true;
            }

            map.remove(key);

            return true;
        }

        return false;
    }
}
