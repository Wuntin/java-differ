/*
 * Copyright 2019 Magnus Hultin
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.hultin.differ;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

public class DifferInheritanceTest {
    private ChildTestObject objectOne = new ChildTestObject("one");
    private ChildTestObject objectTwo = new ChildTestObject("two");
    private ChildTestObject otherObjectOne = new ChildTestObject("one");
    private ChildTestObject objectThree = new ChildTestObject("three");

    @Test public void testDiffReflection() throws IllegalAccessException {
        Collection<ChildTestObject> oldSet = oldSet();
        Collection<ChildTestObject> newSet = newSet();
        Differ<ChildTestObject> d = new Differ<>();
        DiffResult<ChildTestObject> r = d.diff(oldSet, newSet);
        Collection<ChildTestObject> removed = r.getRemovedItems();
        Collection<ChildTestObject> added = r.getAddedItems();

        assertEquals("One item should be removed", 1, removed.size());
        assertEquals("Object Two should be removed", objectTwo, removed.iterator().next());

        assertEquals("One item should be added", 1, added.size());
        assertEquals("Object Three should be added", objectThree, added.iterator().next());
    }

    private Collection<ChildTestObject> oldSet() {
        Collection<ChildTestObject> set = new HashSet<>();

        set.add(objectOne);
        set.add(objectTwo);

        return Collections.unmodifiableCollection(set);
    }

    private Collection<ChildTestObject> newSet() {
        Collection<ChildTestObject> set = new HashSet<>();

        set.add(otherObjectOne);
        set.add(objectThree);

        return Collections.unmodifiableCollection(set);
    }
}
