    /*
 * Copyright 2019 Magnus Hultin
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package net.hultin.differ;

import java.io.Serializable;

class SerializableTestObject implements Serializable {
    private String value;

    public SerializableTestObject() {
    }

    public SerializableTestObject(String value) {
        this.value = value;
    }

    public boolean equals(Object ob) {
        if (ob == null) {
            return false;
        }

        if (! (ob instanceof SerializableTestObject)) {
            return false;
        }

        SerializableTestObject other = (SerializableTestObject)ob;

        return this.value.equals(other.value);
    }
}
